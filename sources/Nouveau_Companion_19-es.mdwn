[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/[[EN|Nouveau_Companion_19]]/ES/[[FR|Nouveau_Companion_19-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Informe irregular sobre el desarrollo de Nouveau


## Edición del 12 de Mayo


### Introducción

Bienvenidos de nuevo al número 19 del TiNDC. Parece que el gestor de memoria TTM ya fué enviado para su revisión en la lista de correo del kernel Linux. Si todo marcha bien se acbará incluyendo en el kernel. Desafortunadamente para nosotros, parece que el TTM cuenta con una única cola FIFO en el espacio del kernel, mientras que nouveau dispone de al menos 16 colas FIFO en el espacio de usuario. 

Entonces, ¿qué?: 

* pediremos cambios al núcleo del código del TTM. 
* necesitaremos implementar el código del driver nouveau que intermedie con el TTM. 
* necesitaremos implementar el código que use el TTM en el espacio de usuario. 
De paso, se ha incluído con el parche del TTM el código para el establecimiento de modo en el espacio del kernel. Este tema ya lo tratamos en un TiNDC anterior. 

La revisión ha sido fundamentalmente positiva, pero es necesario realizar algunos cambios en el área de ioctl(), el estilo de codificación y la documentación. El código está de nuevo en manos de los hackers de Xorg / DRM y sufrirá modificaciones antes de volver a la lista del kernel para su nueva revisión. Bien, ya que parece que la gente de #dri-devel quieren realizar un rediseño más amplio. En resumen, parece que nos esperan más retrasos en el horizonte. 

Nuestro proyecto Vacaciones de Código empezará a principio de junio y durará hasta agosto. Ahuillet, que va a procurar la mejora de nuestro soporte de xv, ha accedido a proporcionarnos un informe de su progreso en cada número del TiNDC que se publique durante ese periodo. ¡Gracias Ahuillet! 

Parece que Nvidia ha comenzado a recortar el soporte de XvMC en las tarjetas G8x, e incluso puede eliminar la implementación, por lo que podría convertirse en nuestra primera "característica exclusiva de nouveau". 

El avance en estos momentos es lento, puesto que tanto marcheu como darktama están bastante ocupados con sus cosas, aunque, con el comienzo del próximo mes se podría regresar a una buena marcha. 

Todavía esperamos la aceptación del proyecto en la [[Software Freedom Conservancy|http://conservancy.softwarefreedom.org/]]. Por lo tanto, nuestros desarrolladores siguen actuando por cuenta propia en lo que se refiere a la compra de hardware, pero poco a poco se han ido incorporando tarjetas G8x en nuestro haber :) . No esperéis un driver funcional en breve ya que, como se explicará más adelante, queda mucho por hacer. 


### Estado actual

En el anterior número advertimos que el porte a BSD se encontraba bloqueado debido a que su DRM carecía de algunas funcionalidades importantes. Incluso antes de que se publicase el último número, camje_lemon probó suerte para hacer funcionar nouveau, y, poco después de publicar el anterior número, tuvo éxito. Por ello pueden alegrarse todos los forofos de *BSD (y ayudarnos a portar y hacer pruebas). 

jwstolk prosiguió con su trabajo en detector de macros. Después de triturar un buen número de fallos, pidió a pq y z3ro lo probasen. PQ hizo algunas sugerencias para mejorar el diseño existente y jwstolk los tuvo en cuenta. A marcheu le gustó la idea. 

Ahuillet inició sus preparativos para el proyecto Vacation of Code, patrocinado por Xorg. Echó un vistazo al código y manifestó una ignoracia absoluta sobre los asuntos tratados, aunque, a continuación, hizo algunas preguntas muy precisas que ponen en tela de juicio su grado de ignorancia. 

Tenemos una lista de correo [[aquí|http://lists.freedesktop.org/mailman/listinfo/nouveau]]. Puedes enviarnos por correo fallos y cualquier otra cuestión relacionada con nouveau. CJB fue lo suficientemente insistente como para ponerla en marcha. De todos modos, nuestra atención principal se centrará de momento en el canal de IRC, aunque, haced uso de ella, por favor. 

Se han resuelto finalmente los problemas con las tarjetas NV15: al intentar emitir instrucciones de renderizado 3D (por ejemplo, corriendo glxgears) la máquina se bloqueaba totalmente. Era debido a una tormenta de interrupciones que dejaban abatido el sistema. Tras algunos tanteos, matc localizó el error: se inicializaba el contexto incorrectamente. En vez de un tamaño de 32 bits usábamos uno de 64 bits. Abotezatu se ofreció a comprobar el parche y comunicó su éxito. Pocos días después pmdata probó suerte y también con éxito, pero ahora las X fallan al ejecutar glxgears y hacer click fuera de su ventana. 

pq y z3ro prosiguieron rematando su especificación de la base de datos rules-ng. Ahora disponemos de marcas de documentación. El trabajo posterior irá encaminado a la creación de herramientas que hagan uso de esta especificación. La discusión se trasladó a #dri-devel, ya que el equipo de radeon desea usar rules-ng también para su hardware. 

Darktama acabó por picar el anzuelo (buen provecho) y compró un equipo nuevo con una tarjeta NVidia 8600 GT. En una primera inspección de las trazas de mmio se vió que las tarjetas 8600 son muy distintas a las anteriores. Cuando Wallbraker ofreció volcados de su nueva y reluciente 8800 GTS también se pudo ver que además existen algunas pequeñas diferencias entre las G80 y las G84. 

Parece que, o bien el driver (beta) actual tiene problemas con algunos programas de vértices (por ejemplo, vertex_program3() en renouveau), o no somos capaces de escribir programas de vértices correctamente. La consecuencia es que renouveau salta al ejecutar vertex_program3(). Se puede comentar ese test con seguridad si el test hace saltar renouveau en tu tarjeta G8x. 

En estos momentos no funciona nada relacionado con esas tarjetas. Se empezará trabajando sobre las funcionalidades 2D. Las 3D son otra historia. Estamos bastante convencidos de que, o lo hacemos correctamente y descubrimos cómo trabajan las nuevas tarjetas, o utilizamos un modo de compatibilidad (con NV4x) que conducirá a resultados con más rapidez, pero que se convertirá en un callejón sin salida a la hora del futuro desarrollo del driver. 

De nuevo, se ha producido cierto desarrollo en la rama de Randr1.2. Airlied añadió nuevo código de inicialización, eliminó código sin uso y, posiblemente, eliminó algunos fallos. 


### Ayuda necesaria

Por favor, enviad volcados de renouveau de las tarjetas SLI y 8x00. Desgraciadamente no tenemos una lista actualizada de volcados, pero los que se envíen no se perderán. Además, estamos a la búsqueda de volcados de MMio de aquellas tarjetas en las que no funcione correctamente glxgears. 

Si tienes una NV15 prueba, por favor, los últimos cambios. 
