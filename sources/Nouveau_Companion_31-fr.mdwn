[[!table header="no" class="mointable" data="""
 [[Accueil|FrontPage-fr]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[Archives anciennes|IrcChatLogs-fr]]  |  [[Archives récentes|IrcChatLogs-fr]]  | DE/[[EN|Nouveau_Companion_31]]/[[ES|Nouveau_Companion_31-es]]/[[FR|Nouveau_Companion_31-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Le compagnon irrégulier du développement de Nouveau (TiNDC)


## Édition du 23 Novembre 2007


### Introduction

Chers lecteurs, bienvenue à une nouvelle édition du TiNDC. La dernière édition a attiré l'œil de quelques sites importants technologiques en Allemagne. En fait, c'était un peu prématuré de dire à ce moment là que la 2D était presque terminée, nous avons maintenant presque tout de fait, excepté le _mode setting_ (et le meilleur support Randr 1.2 qui va avec). Pour l'instant vous pouvez être chanceux sur NV4x et NV3x, mais les cartes plus récentes et les NV5x ne marchent pas très bien. 

* [[http://www.golem.de/0711/55966.html|http://www.golem.de/0711/55966.html]] (en allemand)  
[[http://www.pro-linux.de/news/2007/11966.html|http://www.pro-linux.de/news/2007/11966.html]] (en allemand)  
Alors que le travail sur la partie 2D cesse peu à peu (à l'exception de ce qui est mentionné ci-dessus), l'attention se porte maintenant sur la 3D. Darktama et jkolb travaillent sur Gallium et les problèmes de DRM / TTM. Marcheu se bat toujours avec les plus vieilles cartes sur le framework Gallium3D. Ahuillet, pmdata et p0g attendent qu'ils aient fini afin de pouvoir enfin commencer à coder les fonctionnalités 3D. 


### État actuel

Marcheu a été occupé les dernières semaines, il a travaillé avec Benh et avec les différents testeurs sur les bugs obtenus sur l'architecture PPC. Lors des DFS ([[DownloadFromScreen|DownloadFromScreen]]) / UTS ([[UploadToScreen|UploadToScreen]]), les transferts DMA se bloquaient mais le DMA continuait tout de même à fonctionner, du moment que l'AGP n'était pas activé. IFC (ImageFromCPU) avait comme code erreur l'inverse de ce qui était attendu, et indiquait donc une erreur alors qu'en fait tout s'était bien passé. Ce bug a été repéré par Benh qui a continué à nous aider afin de faire marcher PPC pour tous nos testeurs. De plus, Marcheu a eu accès à distance à une machine PPC pour faire des tests. Il attend actuellement quelques dumps de registres qui bloquent et plantent la machine de manière inattendue. Il a donc besoin qu'une personne soit devant le clavier de la machine distante. 

Ces dumps permettrons peut-être de permettre à Nouveau de marcher sur la machine de chimaera, qui a rapporté toujours subir des bloquages lors du démarrage de Nouveau sur PowerPC. Après avoir échangé quelques patches expérimentaux, marcheu a réussi à réparer ce bug, pour être à nouveau bloqué un peu plus tard lors du démarrage. Il s'est avéré que c'était dû à quelques bugs d'endianness dans le parser de BIOS. Le bug a donc été transmis à malc0 qui a réparé quelques bugs dans son parser. 

Les corrections du parser de BIOS afin de prendre en compte l'endianness, ainsi qu'un peu de ménage du code (qu'on peut appeler désobfuscation) ont été soumis par malc0. Avec stillunknown, ils ont en suite fait le ménage dans le code de randr 1.2 et ont corrigé quelques autres bugs. 

Stillunknown est encore en train d'essayer de faire marcher randr 1.2 sur la carte de AndrewR. Plusieurs patchs de test ont été échangés, mais randr 1.2 ne marche toujours pas pour la carte NV4x de AndrewR. 

Bien que stillunknown ait trouvé de nombreuses erreurs, il a discuté avec Thunderbird et ils ont tout les deux comparés leurs connaissances à propos des différents registres controllant l'affichage à l'écran. Cela les a aidé tout les deux, mais les patchs que stillunkown a alors fait ne marchaient toujours pas pour AndrewR. Nos experts du _mode settings_ vont-ils enfin avoir droit à leur heure de gloire sur le projet Nouveau ? Ne ratez pas notre prochaine édition du TiNDC où tout vous sera révélé (en tout cas un peu plus :) ). 

Thunderbird a décidé de se mettre au reverse engineering de la sortie TV. Il a ajouté une demande de testeurs à notre page « Testers wanted ». 

jb17some a osé se battre avec les périls de XvMC. Bien qu'aucun signe de vie n'ait été aperçu de lui sur IRC, sa page de wiki a été mise à jour. D'après cette mise à jour, il a écrit un petit programme qui se comporte comme si il utilisait XvMC. C'est-à dire que les données en entrée sont mises dans un buffer, les registres sont écrits et le résultat final est une image qui pourrait être affichée. En comparant la sortie et l'état des registres, son programme génère les mêmes résultats que ce que le driver nv dans les mêmes circonstances. 

Notez bien que ce programme de teste n'affiche rien du tout. 

Hum, revenons à marcheu et voyons ce qu'il fait : il semble occupé à écrire le framework Gallium3D pour les anciennes cartes. Essayons à nouveau un peu plus tard. 

Après que ahuillet se soit fait battre par l'équipe NV3x dans la course au premier à faire marcher A8+A8 (et par la même occasion l'accélération EXA), il voulait éviter de se faire appeler _looser_ pour ne pas avoir été capable de réaliser cette tâche simple ;) 

Sérieusement, cette tâche était loin d'être simple. Pour accélérer le [[PictOpt|PictOpt]], plusieurs conditions (par exemple, limites de largeur et limite de position de destination) devaient être réunies. Après avoir parlé avec [[IronPeter|IronPeter]] et Marcheu de la manière de faire ça avec les combinateurs de registres, ahuillet a implémenté un immonde patch, qui a réussi à marcher après quelques tests et patchs. 

[[!img http://nouveau.freedesktop.org/wiki/Nouveau_Companion_31?action=AttachFile&do=get&target=bad.png] 

_(A8+A8 ne marchant pas très bien) _ 

Un jour plus tard, il a prétendu 1) que tout marchait bien et 2) qu'il avait remanié le patch en quelque chose de lisible. Le bon fonctionnement de EXA a été confirmé par p0g et AndrewR. 

Quelques jours plus tard, nous avons eu des rapports que Xv ne marchait pas sur PPC et qu'il y avait quelques problèmes de corruption des polices. Puisque marcheu était parti à la chasse d'une pléthore de bugs PPC (voir plus haut), il a été décidé que nous allions d'abord éliminer les bugs basiques, et que si le problème (bug) avec Xv était toujours présent après ça, ahuillet irait le débugger. Cependant, les problèmes de polices sont en haut de la _TODO list_ de ahuillet. 

[[!img http://nouveau.freedesktop.org/wiki/Nouveau_Companion_31?action=AttachFile&do=get&target=victory.png] 

_(Ça fonctionne, au moins sur x86)_ 

Darktama a effectué un peu de ménage dans le DRM et a saisi cette chance pour introduire un bug 64bits qui empêchait à Nouveau de fonctionner sur x86. Marcheu a découvert et réparé ça quelques heures plus tard. 

jkolb travaille encore sur TTM. 

Donc nous allons peut-être pouvoir sortir une version finale du driver 2D de marcheu (Essayons de lui demander... oh, ok). Il semble qu'il travaille afin de séparer le code Xv en une partie générique et une partie spécifique à chaque carte. Cela devrait nous aider à ajouter un code Xv à base de shaders sur NV4x et NV5x plus facilement. Et nous avons besoin de résoudre les bugs PPC mentionnés précédemment... On dirait que ça va encore demander un peu de temps avant de pouvoir annoncer que la 2D marche. 


### Aide requise

Au nom de l'équipe Nouveau, j'aimerais remercier tous les testeurs qui ont rejoint notre canal IRC et ont offert leur aide. Quelques problèmes ont été résolus, d'autres sont en train d'être étudiés. Avec quelques problèmes ayant besoin de plusieurs passages du problème au patch à un (nouveau) problème, quelques testeurs ont montré un vrai dévouement. 

stillunknown cherche des possesseurs de 7xx0 qui voudraient bien l'aider à faire quelques tests pour lui. De plus, il cherche des personnes ayant du _dual head_ (quelle que soit la combinaison VGA/DVI) qui seraient prêts à tester randr 1.2 et le _mode setting_. 

Marcheu a besoin de testeurs avec des NV30 (pas de modèles plus récents) car les choses sont un peu différentes sur cette carte. 

Et si vous êtes intéressés pour aider Thuderbird et sa sortie TV, jetez un œil ici : [[http://nouveau.freedesktop.org/wiki/TestersWanted|http://nouveau.freedesktop.org/wiki/TestersWanted]] Cette page va également contenir des demandes de tests/d'aide de la part d'autres membres de l'équipe. Donc jetez-y un œuil, nous avons toujours besoin de testeurs ! 

[[<<Édition précédente|Nouveau_Companion_30-fr]] [[Édition suivante >>|Nouveau Companion 32-fr]] 
