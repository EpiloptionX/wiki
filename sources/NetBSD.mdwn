NetBSD has preliminary support for NVIDIA hardware via the `nouveau(4)`
DRM/KMS kernel driver ported from Linux 3.15, and all the same userland
components as for Linux -- the `libdrm` library, the `libdrm_nouveau`
library, the `xf86-video-nouveau` X.org video driver, and the Mesa
`nouveau_dri.so` GL driver.

On NetBSD 7, the kernel driver `nouveau(4)` is available on `i386` and
`amd64`, but disabled by default.  Most bug fixes are pulled up to the
`netbsd-7` branch.   The userland components are not
available in the standard distribution sets, though they are available
through pkgsrc in `x11/xf86-video-nouveau` and `graphics/MesaLib`.

On NetBSD-current, the kernel driver `nouveau(4)` is enabled by default
in the `i386/GENERIC`, `amd64/GENERIC`, and `evbarm/TEGRA` kernel
configurations.  The userland components are available in the
distribution sets, spread across `xbase.tgz`, `xcomp.tgz`, `xserver.tgz`, and
`xdebug.tgz`, or from pkgsrc in `x11/xf86-video-nouveau` and
`graphics/MesaLib`.

Some hardware has been tested, but I don't have a list of it at the
moment.  If the display blanks or otherwise fails when you try to boot
with NVIDIA graphics hardware, you can disable `nouveau(4)` at the boot
prompt by booting with `boot -c` and then typing `disable nouveau` and
`quit` at the following userconf prompt:

    > boot -c
    userconf> disable nouveau
    userconf> quit
    ...

If you have any NetBSD-specific questions, ask on the [tech-x11](mailto:tech-x11@NetBSD.org) mailing list (see [archive](https://mail-index.netbsd.org/tech-x11/tindex.html)).
